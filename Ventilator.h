//
// Created by dojo on 12/04/15.
//

#pragma once

#include <memory>
#include <mutex>
#include "MagicDevice.h"
#include "Ventilator.h"
#include "OilPumpDriver.h"
#include "BrakeDriver.h"
#include "Driver.h"
#include "Counter.h"
#include "VentilatorDriver.h"

class Ventilator: public Driver {

public:
    Ventilator();

    virtual void stop() override;
    virtual void start() override;
    virtual int getCoolDownTime() const override;

private:
    std::shared_ptr<Driver> m_device;
    std::shared_ptr<Driver> m_oilPumpFirst;
    std::shared_ptr<Driver> m_oilPumpSecond;
    std::shared_ptr<Driver> m_brakes;
    std::shared_ptr<Driver> m_ventilatorDriver;
    std::mutex m_mutex;

    friend class Facade;
};

