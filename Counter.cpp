//
// Created by dojo on 13/04/15.
//

#include "Counter.h"
#include <iostream>

using namespace std;

Counter::Counter(std::shared_ptr<Driver> p_driver):m_driver(p_driver) {
}

void Counter::start() {
    m_starts++;
    cout<<"Start counter: "<<m_starts<<" ";
    m_driver->start();
}

void Counter::stop() {
    m_stops++;
    cout<<"Stop counter: "<<m_stops<<" ";
    m_driver->stop();
}

