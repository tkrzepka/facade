//
// Created by dojo on 12/04/15.
//

#include "Ventilator.h"
#include <iostream>

using namespace std;

Ventilator::Ventilator() {
    shared_ptr<Driver> device = make_shared<MagicDevice>();
    m_device = make_shared<Counter>(device);
    shared_ptr<Driver> oilPumpFirst = make_shared<OilPumpDriver>();
    m_oilPumpFirst = make_shared<Counter>(oilPumpFirst);
    shared_ptr<Driver> oilPumpSecond = make_shared<OilPumpDriver>();
    m_oilPumpSecond = make_shared<Counter>(oilPumpSecond);
    shared_ptr<Driver> brakes = make_shared<BrakeDriver>();
    m_brakes = make_shared<Counter>(brakes);
    shared_ptr<Driver> vent = make_shared<VentilatorDriver>();
    m_ventilatorDriver = make_shared<Counter>(vent);
}

int Ventilator::getCoolDownTime() const {
    return m_ventilatorDriver->getCoolDownTime();
}

void Ventilator::stop() {
    m_ventilatorDriver->stop();
}

void Ventilator::start() {
    m_ventilatorDriver->start();
}

