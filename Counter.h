//
// Created by dojo on 13/04/15.
//

#pragma once

#include <memory>
#include "Driver.h"

class Counter:public Driver {
public:
    Counter(std::shared_ptr<Driver> p_driver);

    void start() override;
    void stop() override;

private:
    std::shared_ptr<Driver> m_driver;
    unsigned long m_starts{0};
    unsigned long m_stops{0};

};
