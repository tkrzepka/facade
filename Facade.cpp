//
// Created by dojo on 12/04/15.
//

#include "Facade.h"
#include <iostream>
#include <chrono>

using namespace std;

Facade::Facade(int p_numberOfVentilators): m_numberOfVentilators(p_numberOfVentilators) {
    while(p_numberOfVentilators--)
        m_ventilators.push_back(make_shared<Ventilator>());
}

void Facade::turnOnVentilator(int p_ventilatorNumber) {
    if(p_ventilatorNumber > m_numberOfVentilators){
        cout<<"Such Ventilator does not exists"<<endl;
    }else{
        m_threads.emplace_back([this, p_ventilatorNumber]{
            cout<<"starting ventilator: "<<p_ventilatorNumber<<endl;
            shared_ptr<Ventilator> ventilator( m_ventilators[p_ventilatorNumber] );

            ventilator->m_mutex.lock();

            ventilator->m_oilPumpFirst->start();
            ventilator->m_oilPumpSecond->start();
            ventilator->m_brakes->stop();
            ventilator->start();
            ventilator->m_device->start();
            this_thread::sleep_for(chrono::seconds(
                    ventilator->m_device->getCoolDownTime()));
            ventilator->m_device->stop();

            ventilator->m_mutex.unlock();
            cout<<"ventilator: "<<p_ventilatorNumber<<" is running"<<endl;
        });
    }
}

void Facade::turnOffVentilator(int p_ventilatorNumber) {
    if(p_ventilatorNumber > m_numberOfVentilators){
        cout<<"Such Ventilator does not exists"<<endl;
    }else{
        m_threads.emplace_back([this, p_ventilatorNumber]{
            cout<<"stopping ventilator: "<<p_ventilatorNumber<<endl;
            shared_ptr<Ventilator> ventilator( m_ventilators[p_ventilatorNumber] );

            ventilator->m_mutex.lock();

            ventilator->stop();
            ventilator->m_brakes->start();
            this_thread::sleep_for(chrono::seconds(
                    ventilator->getCoolDownTime()));
            ventilator->m_oilPumpFirst->stop();
            ventilator->m_oilPumpSecond->stop();

            ventilator->m_mutex.unlock();
            cout<<"ventilator: "<<p_ventilatorNumber<<" stopped"<<endl;
        });
    }
}

Facade::~Facade() {
    for(auto& th: m_threads)
        th.join();
}
