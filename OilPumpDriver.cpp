//
// Created by dojo on 12/04/15.
//

#include "OilPumpDriver.h"
#include <iostream>

using namespace std;

void OilPumpDriver::start(){
    cout<<"starting Oil Pump"<<endl;
    m_running=true;
}
void OilPumpDriver::stop(){
    cout<<"stopping Oil Pump"<<endl;
    m_running=false;
}