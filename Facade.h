//
// Created by dojo on 12/04/15.
//

#pragma once
#include <vector>
#include <thread>
#include <list>
#include "Ventilator.h"

class Facade {
public:
    Facade(int p_numberOfVentilators);
    ~Facade();

    void turnOnVentilator(int p_ventilatorNumber);
    void turnOffVentilator(int p_ventilatorNumber);

private:
    std::vector<std::shared_ptr<Ventilator>> m_ventilators;
    const int m_numberOfVentilators;
    std::list<std::thread> m_threads;
};
