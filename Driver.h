//
// Created by dojo on 13/04/15.
//

#pragma once

class Driver {
public:
    virtual void stop() = 0;
    virtual void start() = 0;
    virtual int getCoolDownTime() const{return 0;}
};
