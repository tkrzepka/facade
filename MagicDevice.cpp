//
// Created by dojo on 12/04/15.
//

#include "MagicDevice.h"
#include <iostream>

using namespace std;

void MagicDevice::start(){
    cout<<"starting MagicDevice"<<endl;
    m_on=true;
}
void MagicDevice::stop(){
    cout<<"stopping MagicDevice"<<endl;
    m_on=false;
}

int MagicDevice::getCoolDownTime() const{
    return m_ignitionTime;
}
