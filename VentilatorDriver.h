//
// Created by dojo on 13/04/15.
//

#pragma once

#include "Driver.h"

class VentilatorDriver: public Driver {

public:
    virtual void stop() override;

    virtual void start() override;

    virtual int getCoolDownTime() const override;

private:
    bool m_on{false};
    int m_ventilatorCoolDown{3};
};
