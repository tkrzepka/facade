//
// Created by dojo on 12/04/15.
//

#pragma once

#include "Driver.h"

class MagicDevice: public Driver {
public:
    void start() override;
    void stop() override;
    virtual int getCoolDownTime() const override;

private:
    bool m_on{false};
    int  m_ignitionTime{1};
};
