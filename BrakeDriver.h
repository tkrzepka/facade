//
// Created by dojo on 12/04/15.
//

#pragma once

#include "Driver.h"

class BrakeDriver: public Driver {
public:
    void start() override;
    void stop() override;

private:
    bool m_on{true};
};

