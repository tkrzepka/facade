#include <iostream>
#include <chrono>
#include "Facade.h"

using namespace std;

int main() {
    Facade facade(10);
    facade.turnOnVentilator(1);
    facade.turnOnVentilator(2);
    facade.turnOnVentilator(3);
    facade.turnOnVentilator(4);
    facade.turnOnVentilator(5);
    this_thread::sleep_for(chrono::milliseconds(10));
    facade.turnOffVentilator(1);
    facade.turnOffVentilator(2);
    facade.turnOffVentilator(3);
    this_thread::sleep_for(chrono::milliseconds(10));
    facade.turnOnVentilator(1);
    facade.turnOnVentilator(2);
    facade.turnOnVentilator(3);
    this_thread::sleep_for(chrono::milliseconds(10));
    facade.turnOffVentilator(1);
    this_thread::sleep_for(chrono::milliseconds(10));
    facade.turnOnVentilator(1);
    return 0;
}