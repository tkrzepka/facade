//
// Created by dojo on 13/04/15.
//

#include <iostream>
#include "VentilatorDriver.h"

using namespace std;

void VentilatorDriver::stop() {
    cout<<"stopping Ventilator"<<endl;
    m_on=false;
}

void VentilatorDriver::start() {
    cout<<"starting Ventilator"<<endl;
    m_on=true;
}

int VentilatorDriver::getCoolDownTime() const {
    return m_ventilatorCoolDown;
}
